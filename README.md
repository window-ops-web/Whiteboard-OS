# Whiteboard OS

Whiteboard OS is a free, open-source and lightweight web-based operating system designed specifically for teachers and educators. Please note that small screens are not supported.
